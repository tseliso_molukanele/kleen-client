'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
        'ngRoute',
        'ngAnimate',
        'ui.bootstrap',
        'angularMoment',
        'angular-loading-bar',
        'ui-notification',
        'uiGmapgoogle-maps',
        'myApp.PaymentModule',
        'myApp.ExpenseModule',
        'myApp.InvoiceModule',
        'myApp.AccountController',
        'myApp.ScheduleController',
        'myApp.AccountformController',
        'myApp.AdminController',
        'myApp.AuthModule',
        'myApp.ReportModule',
        'myApp.DashboardModule',
        'myApp.Top10ReportModule',
        'myApp.PlanModule',
    ])
    .config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider) {

        $httpProvider.defaults.useXDomain = true;

        delete $httpProvider.defaults.headers.common['X-Requested-With'];

        $routeProvider.otherwise({redirectTo: '/auth'});

        $httpProvider.interceptors.push('errorInterceptor');
        $httpProvider.interceptors.push('securityInterceptor');
    }])
    .factory('errorInterceptor', function($q, $rootScope, $location) {
        return {
            'responseError': function(rejection) {
                if (rejection.status === 401 || rejection.status === 403) {
                    $rootScope.$broadcast('logoutRequest');
                }
                return $q.reject(rejection);
            }
        };
    })
    .factory('securityInterceptor', function(authInfoService) {
        return {
            'request': function($config) {

                $config.headers['x-auth-token'] = authInfoService.logInfo.xAuthToken;

                return $config;
            }
        };
    })
    .factory('authInfoService', function() {
        return {
            logInfo: {
                loggedInUser: {username: "logged out", roles: []},
                loggedIn: false,
                xAuthToken: ""
            }
        };
    })
    .controller('rootController', ["$scope", "$location", "Notification", "authService", "authInfoService",
        function($scope, $location, Notification, authService, authInfoService) {

            $scope.logInfo = authInfoService.logInfo;

            $scope.menu = {
                forward: "/auth",
                items: {
                    schedule: {
                        forward: "/schedule",
                        items: {}
                    },
                    payments: {
                        forward: null,
                        items: {
                            capture: {
                                forward: "/payment",
                                items: {}
                            },
                            list: {
                                forward: "/payments",
                                items: {}
                            }
                        }
                    },
                    expenses: {
                        forward: null,
                        rolesAllowed: ['ROLE_OFFICE'],
                        items: {
                            capture: {
                                forward: "/expense",
                                rolesAllowed: ['ROLE_OFFICE'],
                                items: {}
                            },
                            list: {
                                forward: "/expenses",
                                rolesAllowed: ['ROLE_OFFICE'],
                                items: {}
                            }
                        }
                    },
                    admin: {

                        forward: "/admin",
                        rolesAllowed: ['ROLE_ADMIN'],
                        items: {}

                    },
                    plans: {
                        forward: "/plans",
                        rolesAllowed: ['ROLE_ADMIN', 'ROLE_SALES'],
                        items: {}

                    },
                    account: {
                        forward: null,
                        items: {
                            search: {
                                forward: "/account/search",
                                items: {}
                            },
                            new: {
                                forward: "/accountform/new",
                                rolesAllowed: ['ROLE_SALES', 'ROLE_FIELD', 'ROLE_OFFICE'],
                                items: {}
                            },
                            dashboard: {
                                forward: "/dashboard",
                                rolesAllowed: ['ROLE_SALES', 'ROLE_FIELD', 'ROLE_OFFICE'],
                                items: {}
                            },
                            'search & browse': {
                                forward: "/report/top10",
                                rolesAllowed: ['ROLE_SALES', 'ROLE_ADMIN'],
                                items: {}
                            }
                        }
                    },
                    report: {
                        forward: null,
                        rolesAllowed: ['ROLE_ADMIN'],
                        items: {
                            incomeInvoice: {
                                forward: "/report/incomeInvoice",
                                rolesAllowed: ['ROLE_ADMIN'],
                                items: {}
                            },
                            income: {
                                forward: "/report/income",
                                rolesAllowed: ['ROLE_ADMIN'],
                                items: {}
                            },
                            dailyexpense: {
                                forward: "/report/dailyexpense",
                                rolesAllowed: ['ROLE_ADMIN'],
                                items: {}
                            },
                            top10: {
                                forward: "/report/top10",
                                rolesAllowed: ['ROLE_SALES', 'ROLE_ADMIN'],
                                items: {}
                            }
                        }
                    }
                }
            };

            var prepareMenuItem = function(root) {

                if (!root || !Object.keys(root.items).length) {

                    return;
                }

                if (!root.isHidden) {

                    root.isHidden = function() {

                        return root.hidden || (root.rolesAllowed && !authService.hasRoles(root.rolesAllowed));
                    }
                }

                for (var key in root.items) {

                    var item = root.items[key];

                    !function outer(item) {

                        item.parent = root;
                        item.key = key;
                        item.isHidden = function() {

                            return item.parent.isHidden() || item.hidden || (item.rolesAllowed && !authService.hasRoles(item.rolesAllowed));
                        };

                        prepareMenuItem(item);
                    }(item);
                }
            };

            $scope.menu.key = "Main";
            $scope.menu.hidden = true;

            $scope.$on("loggedOutEvent", function(event) {

                $scope.menu.hidden = true;
                $location.path('/auth');
            });

            prepareMenuItem($scope.menu);

            $scope.currentMenuItem = $scope.menu;

            $scope.activateParent = function(item) {

                if (item.forward) {

                    $location.path(item.forward);
                }
            };

            $scope.activateChild = function(item) {

                if (Object.keys(item.items).length) {

                    $scope.currentMenuItem = item;
                } else {

                    $scope.activateParent(item);
                }
            };

            $scope.menuBack = function() {

                if ($scope.currentMenuItem.parent) {
                    $scope.currentMenuItem = $scope.currentMenuItem.parent;
                }
            };

            $scope.$on("loggedInEvent", function(event) {

                var hasAdmin = false;
                var hasOffice = false;
                var hasField = false;
                var hasSales = false;

                for (var index = 0; index < authInfoService.logInfo.loggedInUser.authorities.length; index++) {

                    if ('ROLE_ADMIN' === authInfoService.logInfo.loggedInUser.authorities[index].authority) {

                        hasAdmin = true;
                    } else if ('ROLE_OFFICE' === authInfoService.logInfo.loggedInUser.authorities[index].authority) {

                        hasOffice = true;
                    } else if ('ROLE_FIELD' === authInfoService.logInfo.loggedInUser.authorities[index].authority) {

                        hasField = true;
                    } else if ('ROLE_SALES' === authInfoService.logInfo.loggedInUser.authorities[index].authority) {

                        hasSales = true;
                    }
                }

                if (hasAdmin) {

                    $location.path('/report/incomeInvoice');
                } else if (hasOffice) {

                    $location.path('/account/search');
                } else if (hasField) {

                    $location.path('/schedule');
                } else if(hasSales) {

                    $location.path('/accountform/new');
                }

                $scope.menu.hidden = false;
            });

        }])
    .run(function($rootScope, $templateCache) {
        $rootScope.$on('$routeChangeStart', function(event, next, current) {
            if (typeof(current) !== 'undefined'){
                $templateCache.remove(current.templateUrl);
            }
        });
    })
    .filter('dayofservice', function() {
        return function(input) {
            switch (input) {
                case 2:
                    return "Monday";
                    break;
                case 3:
                    return "Tuesday";
                    break;
                case 4:
                    return "Wednesday";
                    break;
                case 5:
                    return "Thursday";
                    break;
                case 6:
                    return "Friday";
                    break;
                case 0:
                    return "Saturday";
                    break;
                case 1:
                    return "Sunday";
                    break;
                default:
                    return "UNKNOWN";
            }
        }
    })
    .filter('servicefrequency', function() {
        return function(input) {
            switch (input) {
                case 'EVERY_WEEK':
                    return "Weekly";
                    break;
                case 'BI_WEEKLY':
                    return "Fortnightly";
                    break;
                case 'ONCE_PER_MONTH':
                    return "Once per Month";
                    break;
                default:
                    return "UNKNOWN";
            }
        }
    })
    .filter('encodeURIComponent', function() {
        return window.encodeURIComponent;
    })
;
