'use strict';

angular.module('myApp.InvoiceModule', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider  
  .when('/invoice/:id', {
      templateUrl: 'components/invoice/form/InvoiceForm.html',
	  controller: 'InvoiceController'
  });
}])

.controller('InvoiceController', ["$scope", "$http", 'Notification', '$routeParams', '$filter', function($scope, $http, Notification, $routeParams, $filter) {

    $scope.submit = function(invoice) {

            var req = {
                method: 'PUT',
                url: KLEEN_BASE_LOCATION + '/invoice/' + $routeParams.id,
                headers: {
                    'Content-Type': "application/json"
                },
                data: invoice
            }

            $http(req).
            success(function(data, status, headers, config) {
                Notification.success({message: "invoice edit successful", positionX: 'center', positionY: 'bottom'});
            }).
            error(function(data, status, headers, config) {
                Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
            });
        };



    $scope.getInvoice = function(id) {

        var req = {
            method: 'GET',
            url: KLEEN_BASE_LOCATION + '/invoice/' + id
        }

        return $http(req)
            .then(function(response) {

                $scope.invoice = response.data;
            });
    }

    $scope.getInvoice($routeParams.id);

}]);
