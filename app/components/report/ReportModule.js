'use strict';

angular.module('myApp.ReportModule', ['ngRoute', 'n3-line-chart'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/report/dailyexpense', {
                templateUrl: 'components/report/ReportView.html',
                controller: 'DailyExpenseReportController'
            })
            .when('/report/incomeInvoice', {
                templateUrl: 'components/report/ReportView.html',
                controller: 'IIReportController'
            })
            .when('/report/income', {
                templateUrl: 'components/report/ReportView.html',
                controller: 'IncomeReportController'
            });
    }])

    .controller('IIReportController', ["$scope", "Notification", "reportDataService", function($scope, Notification, reportDataService) {

        var req = reportDataService.getIncomeInvoiceReportData().success(function(data, status, headers, config) {

            $scope.data = [];

            for (var item in data) {
                $scope.data.push({
                    x: data[item].date,
                    val_0: data[item].invoiceAmount,
                    val_1: data[item].incomeAmount,
                    val_2: data[item].expenseAmount
                });
            }

            $scope.options = {
                axes: {x: {type: "date"}},
                series: [
                    {
                        y: "val_0",
                        label: "Invoice",
                        color: "#FFaF2c",
                        type: "line"
                    },
                    {
                        y: "val_1",
                        label: "Income",
                        color: "green",
                        type: "column"
                    },
                    {
                        y: "val_2",
                        label: "Expense",
                        color: "#cc0000",
                        type: "column"
                    }
                ], tooltip: {
                    mode: "scrubber",
                    formatter: function(x, y, series) {
                        return moment(x).fromNow() + ' : ' + y;
                    }
                }
            };

        });

        req.error(function(data, status, headers, config) {
            Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
        });

    }])
    .controller('IncomeReportController', ["$scope", "Notification", "reportDataService", function($scope, Notification, reportDataService) {

        var req = reportDataService.getIncomeReportData().success(function(data, status, headers, config) {

            $scope.data = [];

            for (var item in data) {
                $scope.data.push({
                    x: data[item].date,
                    val_0: data[item].postDateAmount,
                    val_1: data[item].preDateAmount,
                    val_2: data[item].incomeAmount,
                    val_3: data[item].postInvoiceAmount,
                    val_4: data[item].preInvoiceAmount,
                    val_5: data[item].cashIncomeAmount,
                    val_6: data[item].bankIncomeAmount
                });
            }

            $scope.options = {
                axes: {x: {type: "date"}},
                series: [
                    {
                        y: "val_0",
                        label: ">> Day",
                        color: "red",
                        type: "line"
                    },
                    {
                        y: "val_1",
                        label: "<< Day",
                        color: "purple",
                        type: "line"
                    },
                    {
                        y: "val_2",
                        label: "Total",
                        color: "black",
                        type: "line"
                    },
                    {
                        y: "val_3",
                        label: ">> Invoice",
                        color: "blue",
                        type: "line"
                    },
                    {
                        y: "val_4",
                        label: "<< Invoice",
                        color: "darkcyan",
                        type: "line"
                    },
                    {
                        y: "val_5",
                        label: "Cash",
                        color: "darkkhaki",
                        type: "line"
                    },
                    {
                        y: "val_6",
                        label: "Bank",
                        color: "green",
                        type: "line"
                    }
                    
                ], tooltip: {
                    mode: "scrubber",
                    formatter: function(x, y, series) {
                        return moment(x).fromNow() + ' : ' + y;
                    }
                }
            };

        });

        req.error(function(data, status, headers, config) {
            Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
        });

    }])
    .controller('DailyExpenseReportController', ["$scope", "Notification", "reportDataService", function($scope, Notification, reportDataService) {

        var req = reportDataService.getExpenseReportData().success(function(data, status, headers, config) {

            $scope.data = [];

            for (var index in data) {

                var item = {
                    x: data[index].date
                };

                item.val_0 = data[index].petrolAmount;
                item.val_1 = data[index].cleaningEquipmentAmount;
                item.val_2 = data[index].wagesAmount;
                item.val_3 = data[index].motorEquipmentAmount;
                item.val_4 = data[index].airtimeAmount;
                item.val_5 = data[index].otherAmount;

                $scope.data.push(item);
            }

            $scope.options = {
                axes: {x: {type: "date"}},
                series: [
                    {
                        y: "val_0",
                        label: "Petrol",
                        color: "darkkhaki",
                        type: "column"
                    },
                    {
                        y: "val_1",
                        label: "Cleaning",
                        color: "green",
                        type: "column"
                    },
                    {
                        y: "val_2",
                        label: "Wages",
                        color: "black",
                        type: "column"
                    },
                    {
                        y: "val_3",
                        label: "Motor",
                        color: "blue",
                        type: "column"
                    },
                    {
                        y: "val_4",
                        label: "Airtime",
                        color: "darkcyan",
                        type: "column"
                    },
                    {
                        y: "val_5",
                        label: "Other",
                        color: "purple",
                        type: "column"
                    }
                ], tooltip: {
                    mode: "scrubber",
                    formatter: function(x, y, series) {
                        return moment(x).fromNow() + ' : ' + y;
                    }
                }
            };

        });

        req.error(function(data, status, headers, config) {
            Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
        });

    }])
    .factory('reportDataService', ['$http', function($http) {

        return {

            getIncomeInvoiceReportData: function() {

                var req = {
                    method: 'GET',
                    url: KLEEN_BASE_LOCATION + '/report/incomeInvoice'
                };

                return $http(req);
            },

            getIncomeReportData: function() {

                var req = {
                    method: 'GET',
                    url: KLEEN_BASE_LOCATION + '/report/income'
                };

                return $http(req);
            },

            getExpenseReportData: function() {

                var req = {
                    method: 'GET',
                    url: KLEEN_BASE_LOCATION + '/report/expense'
                };

                return $http(req);
            }
        }

    }]);

