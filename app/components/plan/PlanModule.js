'use strict';

angular.module('myApp.PlanModule', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/plans', {
      templateUrl: 'components/plan/PlansEditor.html',
      controller: 'PlanEditorController'
  });
}])
.controller('PlanEditorController', ["$scope", "$http", 'planService', function($scope, $http, planService) {

  $scope.currentYear = 2017;

  $scope.changeYear = function(year) {
    $scope.currentYear = year
  }

  $scope.newYear = function() {
    var year = Math.max.apply(null, Object.keys($scope.plans)) + 1;

    $scope.plans[year] = [];

    $scope.currentYear = year
  }

  planService.getPlansByYear()
  .then(function(response) {
      $scope.plans = response;
  });

  $scope.update = function(plan, index) {
    plan.year = $scope.currentYear;
    planService.update(plan)
    .then(function(response) {

      var newPlan = response;
      $scope.plans[$scope.currentYear][index] = newPlan;
    });
  }

  $scope.save = function(plan, index) {
    plan.year = $scope.currentYear;
    planService.save(plan)
    .then(function(response) {

      var newPlan = response;
      $scope.plans[$scope.currentYear][index] = newPlan;
    });
  }

  $scope.newPlan = function() {
    $scope.plans[$scope.currentYear].push({frequency: 'EVERY_WEEK', numberOfBins: 1, price: 30})
  }
}])
.factory('planService', ['$http', function($http) {

        var planService = {};

        planService.getPlansByYear = function() {

          var req = {
              method: 'GET',
              url: KLEEN_BASE_LOCATION + '/plan'
          };

          return $http(req)
          .then(function(response) {

    	       var plans = response.data;

             var plansByYear = {}
             for(var key in plans) {

               if(!plansByYear[plans[key].year]) {

                 plansByYear[plans[key].year] = [];
               }

               plansByYear[plans[key].year].push(plans[key]);
             }

             return plansByYear;
    	    });
        }

        planService.save = function(plan) {

            var req = {
                method: 'POST',
                url: KLEEN_BASE_LOCATION + '/plan',
                headers: {
                    'Content-Type': "application/json"
                },
                data: plan
            };

            return $http(req)
            .then(function(response) {

              return response.data;
            });
        };

        planService.update = function(plan) {

            var req = {
                method: 'PUT',
                url: KLEEN_BASE_LOCATION + '/plan/' + plan.id,
                headers: {
                    'Content-Type': "application/json"
                },
                data: plan
            };

            return $http(req)
            .then(function(response) {

              return response.data;
            });
        };

        return planService;

    }]);
;
