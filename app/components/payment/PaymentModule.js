'use strict';

angular.module('myApp.PaymentModule', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider  
  .when('/payment', {
      templateUrl: 'components/payment/form/PaymentForm.html',
      controller: 'PaymentController'
  })
  .when('/payment/:id', {
      templateUrl: 'components/payment/form/PaymentForm.html',
	  controller: 'PaymentController'
  })
  .when('/payments', {
	  templateUrl: 'components/payment/list/ListPayments.html',
	  controller: 'PaymentListController'
  });
}])

.controller('PaymentListController', ["$scope", "$http", "$location", function($scope, $http, $location) {

	$scope.getAllPayments = function() {
		
		var req = {
				 method: 'GET',
				 url: KLEEN_BASE_LOCATION + '/payment/'
		};
		
	    return $http(req)
	    .then(function(response){

			$scope.allPayments = response.data; 
	    	return response.data;
	    });
	};
	
	$scope.getAllPayments();

    $scope.showPayment = function(id) {
        $location.path('/payment/' + id);
    };

}])
.controller('PaymentController', ["$scope", "$http", 'Notification', '$routeParams', '$filter', 'accountService', function($scope, $http, Notification, $routeParams, $filter, accountService) {

    $scope.$watch('payment.type', function(new_val){
        $scope.payment.reference = new_val;
    });

    $scope.viewFocused = function() {

            if(!$scope.focusedProcessed) {

                datepickr('date', { dateFormat: 'Y-m-d'})
                $scope.focusedProcessed = true;

//			document.querySelector(".calendar").style.display = 'block';
            }
        }

    $scope.getCustomers = accountService.getAccounts;

    $scope.selectCustomer = accountService.getAccounts;

    $scope.submit = function(payment) {

            var req = {
                method: 'POST',
                url: KLEEN_BASE_LOCATION + '/payment',
                headers: {
                    'Content-Type': "application/json"
                },
                data: payment
            }

            if($routeParams.id) {

                req.method = 'PUT';
                req.url = req.url + "/" + $routeParams.id;
            }

            $http(req).
            success(function(data, status, headers, config) {
                
                $scope.getPreviousPayments(data.account.accountNumber)
                .then(function() {
                    
                    Notification.success({message: "payment made to account: " + data.account.accountNumber, positionX: 'center', positionY: 'bottom'});
                    $scope.payment.account = null;
                    $scope.payment.amount = null; 
                })
            }).
            error(function(data, status, headers, config) {
                Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
            });
        };

    $scope.onSelect = function($item, $model, $label) {

        $scope.getPreviousPayments($item.accountNumber);

        $scope.getPreviousInvoices($item.accountNumber);

        $scope.getPreviousServices($item.accountNumber);

        };

    $scope.getPreviousPayments = function(acountNumber) {
        var req = {
            method: 'GET',
            url: KLEEN_BASE_LOCATION + '/account/' + acountNumber + '/payment/'
        };

        return $http(req)
            .then(function(response){

                $scope.payments = response.data;
            });
    }

    $scope.getPreviousInvoices = function(accountNumber) {
        var req = {
            method: 'GET',
            url: KLEEN_BASE_LOCATION + '/account/' + accountNumber + '/invoice/'
        };

        return $http(req)
            .then(function(response){

                $scope.invoices = response.data;
            });
    }

    $scope.getPreviousServices = function(accountNumber) {

        var req = {
            method: 'GET',
            url: KLEEN_BASE_LOCATION + '/account/' + accountNumber + '/service/'
        };

        return $http(req)
            .then(function(response){

                $scope.services = response.data;
            });
    }

    $scope.getPayment = function(id) {

        var req = {
            method: 'GET',
            url: KLEEN_BASE_LOCATION + '/payment/' + id
        }

        return $http(req)
            .then(function(response) {

                $scope.payment = response.data;
            });
    }

    if ($routeParams.id) {

        $scope.getPayment($routeParams.id).then(function() {

            $scope.getPreviousPayments($scope.payment.account.accountNumber);

            $scope.getPreviousInvoices($scope.payment.account.accountNumber);

            $scope.getPreviousServices($scope.payment.account.accountNumber);

            $scope.payment.date = $filter('date')(new Date($scope.payment.date), 'yyyy-MM-dd');
        });
    }
}]);
