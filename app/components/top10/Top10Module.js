'use strict';

angular.module('myApp.Top10ReportModule', [])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when("/report/top10", {
                templateUrl: 'components/top10/Top10View.html',
                controller: 'Top10Controller'
            })
            .when("/account/top10", {
                templateUrl: 'components/top10/Top10View.html',
                controller: 'Top10Controller'
            });
    }])

    .controller('Top10Controller', ["$scope", "Notification", 'accountService', '$location', function($scope, Notification, accountService, $location) {

        $scope.currentPage = 1;
        $scope.pageSize = 20;
        $scope.filteredAccounts = [];
        $scope.pages = [];

        $scope.gotoPage = function(pageNumber) {
            $scope.currentPage = pageNumber;
        }

        $scope.catchData = function (filteredData) {

          $scope.pages = [].constructor(
            Math.ceil(
              (filteredData.length / $scope.pageSize)
            )
          );

          $scope.filteredAccounts = filteredData;
          return filteredData;
        }

        $scope.accounts = [
            {"address":{"displayAddress":"Loading..."},"accountHolder":{"displayName":"Loading..."},"accountNumber":"1","balance":0.0,"status":"Loading..."}];

        var accounts = accountService.getAccounts('', 'ALL').then(function(response) {

            response.forEach(function(account) {
              account.accountNumber = parseInt(account.accountNumber);
              if(account.status === 'INACTIVE') {
                account.status = 'PAUSED';
              }
            });
            $scope.accounts = response;
        });

        $scope.showAccount = function(accNum) {
            $location.path('/account/' + accNum);
        }
    }]);
