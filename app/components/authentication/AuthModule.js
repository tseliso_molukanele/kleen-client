'use strict';

angular.module('myApp.AuthModule', [ 'ngRoute' ])

.config([ '$routeProvider', function($routeProvider) {
	$routeProvider
	.when('/auth', {
		templateUrl : 'components/landing/LandingPage.html',
		controller : 'AuthController'
	});
} ])
.directive('kbRole', ['authService', 'authInfoService', function(authService, authInfoService) {

	return {
		restrict: 'A',
    	link: function link(scope, element, attrs) {

			element.hide();

			scope.$watch(function() {
					return authInfoService.logInfo;
				},
				function(oldValue, newValue) {

				if(authInfoService.logInfo.loggedInUser.loggedIn) {

					if(authService.hasRoles(attrs.kbRole.split(','))) {
						element.show();
					} else {
						element.hide();
					}
				} else {
					element.hide();
				}
			});
  		}
  	};
}]
)
.factory('authService', ["$http", "$rootScope", "Notification", "authInfoService", function($http, $rootScope, Notification, authInfoService) {

		var authService = {

			login : function(username, password) {

                var req = {
				 method: 'POST',
				 url: KLEEN_BASE_LOCATION + "/user/login",
				 headers: {
					 "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
				 },
				 data: "username=" + username + "&password=" + password
				}

		$http(req)
		  .success(function(data, status, headers, config) {

			  	authInfoService.logInfo.loggedInUser = data;
          authInfoService.logInfo.loggedInUser.roles = [];
			  	authInfoService.logInfo.loggedInUser.loggedIn = true;
			  	authInfoService.logInfo.xAuthToken = headers('x-auth-token');

				for (var index = 0; index < authInfoService.logInfo.loggedInUser.authorities.length; index++) {
					authInfoService.logInfo.loggedInUser.roles.push(authInfoService.logInfo.loggedInUser.authorities[index].authority);
				}

				authInfoService.logInfo.loggedInUser.roles.sort();

				$rootScope.$broadcast("loggedInEvent");
		  })
		  .error(function(data, status, headers, config) {
			  if(status === 0) {

                Notification.error({message: 'The server is down!', positionX: 'center', positionY: 'bottom'});
			  } else {

                  Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
			  }

		  });

			},

			logout : function() {

                		var req = {
				 method: 'DELETE',
				 url: KLEEN_BASE_LOCATION + "/user/login"
				}

		$http(req).
		  success(function(data, status, headers, config) {

            authInfoService.logInfo.loggedInUser = {username:"logged out", roles: []};
            authInfoService.logInfo.loggedIn = false;
            authInfoService.logInfo.xAuthToken = "";
		  });

          $rootScope.$broadcast("loggedOutEvent");

			},

			hasRoles : function(roles) {

				// console.log('hasroles');
				// console.log(roles)
				// console.log(authInfoService.logInfo.loggedInUser.roles)

				roles.sort();

				var intersect = this.intersect_safe(authInfoService.logInfo.loggedInUser.roles, roles);
				return intersect.length > 0;
			},

			intersect_safe : function (a, b) {
  				var ai=0, bi=0;
  				var result = new Array();

  				while( ai < a.length && bi < b.length ) {
     				if      (a[ai] < b[bi] ){ ai++; }
     				else if (a[ai] > b[bi] ){ bi++; }
     				else {
       					result.push(a[ai]);
       					ai++;
       					bi++;
     				}
  				}

  				return result;
			}
		};

                $rootScope.$on("logoutRequest", function(event) {

               authService.logout();
            })

		return authService;
}]
)
.controller('AuthController', [ "$scope", "$http", "Notification", "authService", function($scope, $http, Notificatio, authService) {

	$scope.username = "";
	$scope.password = "";

	$scope.login = function(login) {

        authService.login(login.username, login.password);
	};

	$scope.logout = function() {

        authService.logout();
	};
} ]);
