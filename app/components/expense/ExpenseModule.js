'use strict';

angular.module('myApp.ExpenseModule', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider  
  .when('/expense', {
      templateUrl: 'components/expense/form/ExpenseForm.html',
      controller: 'ExpenseController'
  })
  .when('/expense/:id', {
      templateUrl: 'components/expense/form/ExpenseForm.html',
	  controller: 'ExpenseController'
  })
  .when('/expenses', {
	  templateUrl: 'components/expense/list/ListExpenses.html',
	  controller: 'ExpenseListController'
  });
}])

.controller('ExpenseListController', ["$scope", "$http", "$location", function($scope, $http, $location) {

	$scope.getAllExpenses = function() {
		
		var req = {
				 method: 'GET',
				 url: KLEEN_BASE_LOCATION + '/expense/'
		};
		
	    return $http(req)
	    .then(function(response){

			$scope.allExpenses = response.data;
	    	return response.data;
	    });
	};
	
	$scope.getAllExpenses();

    $scope.showExpense = function(id) {
        $location.path('/expense/' + id);
    };

}])
.controller('ExpenseController', ["$scope", "$http", 'Notification', '$routeParams', '$filter', function($scope, $http, Notification, $routeParams, $filter) {

    $scope.viewFocused = function() {

            if(!$scope.focusedProcessed) {

                datepickr('date', { dateFormat: 'Y-m-d'})
                $scope.focusedProcessed = true;

//			document.querySelector(".calendar").style.display = 'block';
            }
        }

    $scope.submit = function(expense) {

            var req = {
                method: 'POST',
                url: KLEEN_BASE_LOCATION + '/expense',
                headers: {
                    'Content-Type': "application/json"
                },
                data: expense
            }

            if($routeParams.id) {

                req.method = 'PUT';
                req.url = req.url + "/" + $routeParams.id;
            }

            $http(req).
            success(function(data, status, headers, config) {
                Notification.success({message: "expense captured", positionX: 'center', positionY: 'bottom'});
                $scope.expense.date = null;
                $scope.expense.amount = null;
                $scope.expense.expenseType = null;
            }).
            error(function(data, status, headers, config) {
                Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
            });
        };

    $scope.getExpense = function(id) {

        var req = {
            method: 'GET',
            url: KLEEN_BASE_LOCATION + '/expense/' + id
        }

        return $http(req)
            .then(function(response) {

                $scope.expense = response.data;
            });
    }

    if ($routeParams.id) {

        $scope.getExpense($routeParams.id).then(function() {

            $scope.expense.date = $filter('date')(new Date($scope.expense.date), 'yyyy-MM-dd');
        });
    }
}]);
