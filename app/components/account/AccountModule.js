'use strict';

angular.module('myApp.AccountController', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/account/:accountNumber', {
	  templateUrl: 'components/account/AccountView.html',
	  controller: 'AccountController'
  });
}])

.controller('AccountController', ["$scope", "$http", '$routeParams', "accountService", "Notification", function($scope, $http, $routeParams, accountService, Notification) {

	$scope.reportLocation = KLEEN_BASE_LOCATION;
	$scope.todayString = moment().format('YYYYMM');

    $scope.getAccounts = accountService.getAccounts;

	$scope.downloadFile = function(fileURL, fileName) {
		$http({
                    url : fileURL,
                    method : 'GET',
                    params : {},
                    responseType : 'arraybuffer'
                }).success(function(data, status, headers, config) {
                    var file = new Blob([ data ], {
                        type : 'application/pdf'
                    });
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    var a         = document.createElement('a');
                    a.href        = fileURL;
                    a.target      = '_blank';
                    a.download    = fileName;
                    document.body.appendChild(a);
                    a.click();
                }).error(function(data, status, headers, config) {

                });
     };

	  $scope.emailStatement = function (account) {

			var request = accountService.sendStatement(account, 'email');
			request
				.success(function (data, status, headers, config) {
                    Notification.success({message: "Email sent successfully!", positionX: 'center', positionY: 'bottom'});
				})
				.error(function (data, status, headers, config) {
					if (data.message) {
                        Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
					} else if (data) {
                        Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
					} else {
                        Notification.error({message: "Error: " + status, positionX: 'center', positionY: 'bottom'});
					}
				});
		}

    $scope.smsStatement = function (account) {

			var request = accountService.sendStatement(account, 'sms');
			request
				.success(function (data, status, headers, config) {
                    Notification.success({message: "SMS sent successfully!", positionX: 'center', positionY: 'bottom'});
				})
				.error(function (data, status, headers, config) {
					if (data.message) {
                        Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
					} else if (data) {
                        Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
					} else {
                        Notification.error({message: "Error: " + status, positionX: 'center', positionY: 'bottom'});
					}
				});
		}

		$scope.loadAccountDetails = function(accountNumber) {
			var req = {
				 method: 'GET',
				 url: KLEEN_BASE_LOCATION + '/account/' + accountNumber + '/payment/'
			};

			$http(req)
		    .then(function(response){

		    	$scope.payments = response.data;
	         });

			var req = {
				 method: 'GET',
				 url: KLEEN_BASE_LOCATION + '/account/' + accountNumber + '/invoice/'
			};

	    	$http(req)
		    .then(function(response){

		    	$scope.invoices = response.data;
                });

			var req = {
				 method: 'GET',
				 url: KLEEN_BASE_LOCATION + '/account/' + accountNumber + '/service/'
			};

	    	$http(req)
		    .then(function(response){

		    	$scope.services = response.data;
                });
		};

		$scope.onselect = function($item, $model, $label) {

		    $scope.loadAccountDetails($item.accountNumber);
		};

		if ($routeParams.accountNumber.toUpperCase() !== 'SEARCH') {

			accountService.getAccount($routeParams.accountNumber).
				success(function (data, status, headers, config) {
					$scope.account = data;
				}).
				error(function (data, status, headers, config) {

					if (status === 404) {
						Notification.error({message: "No account for account number: " + $routeParams.accountNumber, positionX: 'center', positionY: 'bottom'});
					} else if (data.message) {
						Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
					} else {
						Notification.error({message: "Error fetching account!", positionX: 'center', positionY: 'bottom'});
					}
				});

			$scope.loadAccountDetails($routeParams.accountNumber);
		}
}])
.factory('accountService', ['$http', function($http) {

        var accountService = {};

	    accountService.getAccounts = function(key, mode) {

			var req = {
				method: 'GET',
				url: KLEEN_BASE_LOCATION + '/account/list?searchKey=' + key + (mode ? '&mode=' + mode : '')
			};

			return $http(req)
				.then(function(response){

					return response.data;
				});
		};

        accountService.postAccount = function(account) {

            var req = {
                method: 'POST',
                url: KLEEN_BASE_LOCATION + '/account',
                headers: {
                    'Content-Type': "application/json"
                },
                data: account
            };

            return $http(req);
        };

        accountService.putAccount = function(account) {

            var req = {
                method: 'PUT',
                url: KLEEN_BASE_LOCATION + '/account/' + account.accountNumber,
                headers: {
                    'Content-Type': "application/json"
                },
                data: account
            };

            return $http(req);
        };

        accountService.putActivity = function(account, moveUp) {

            var req = {
                method: 'PUT',
                url: KLEEN_BASE_LOCATION + '/account/' + account.accountNumber + '/activity?up=' + moveUp
            };

            return $http(req);
        };

        accountService.sendStatement = function(account, type) {

            var req = {
                method: 'PUT',
                url: KLEEN_BASE_LOCATION + '/account/' + account.accountNumber + '/statement?type=' + type
            };

            return $http(req);
        };

        accountService.getAccount = function(accountNumber) {

            var req = {
                method: 'GET',
                url: KLEEN_BASE_LOCATION + '/account/' + accountNumber
            };

            return $http(req);
        };

        return accountService;
    }]);
