'use strict';

angular.module('myApp.AdminController', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/admin', {
                templateUrl: 'components/admin/AdminView.html',
                controller: 'AdminController'
            });
    }])
    .controller('AdminController', ['adminService', 'Notification', '$scope', function(adminService, Notification, $scope) {

        $scope.viewFocused = function() {

            if (!$scope.focusedProcessed) {

                datepickr('date', {dateFormat: 'Y-m-d'});
                $scope.focusedProcessed = true;

//				document.querySelector(".calendar").style.display = 'block';
            }
        };

        $scope.invoiceSend = function() {

            var request = null;

            request = adminService.invoiceSend();
            request.success(function(data, status, headers, config) {
                Notification.success({message: "Invoice sent successfully!", positionX: 'center', positionY: 'bottom'});
            });


            request
                .error(function(data, status, headers, config) {
                    if (data.message) {
                        Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
                    } else if (data) {
                        Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
                    } else {
                        Notification.error({message: "Error: " + status, positionX: 'center', positionY: 'bottom'});
                    }
                });
        }

        $scope.invoice = function(date, send) {

            var request = null;

            request = adminService.invoice(date, send);
            request.success(function(data, status, headers, config) {
                $scope.payload.date = '';
                Notification.success({message: "Invoice successfully!", positionX: 'center', positionY: 'bottom'});
            });


            request
                .error(function(data, status, headers, config) {
                    if (data.message) {
                        Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
                    } else if (data) {
                        Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
                    } else {
                        Notification.error({message: "Error: " + status, positionX: 'center', positionY: 'bottom'});
                    }
                });
        }

        adminService.getSysteInfo()
        .success(function(data, status, headers, config) {

            $scope.systemInfo = data;
        });

    }])
    .factory('adminService', ['$http', function($http) {

        var adminService = {};

        adminService.invoice = function(date, send) {

            var url = KLEEN_BASE_LOCATION + '/invoice/action/run?date=' + date;
            if(send) {
              url = url + '&send=true';
            }

            var req = {
                method: 'PUT',
                url: url
            };

            return $http(req);
        };

        adminService.invoiceSend = function() {

            var url = KLEEN_BASE_LOCATION + '/invoice/action/send';

            var req = {
                method: 'PUT',
                url: url
            };

            return $http(req);
        };

        adminService.getSysteInfo = function() {

            var url = KLEEN_BASE_LOCATION + '/unsecured/sysinfo';

            var req = {
                method: 'GET',
                url: url
            };

            return $http(req);
        };

        return adminService;
    }]);
