'use strict';

angular.module('myApp.ScheduleController', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider  
  .when('/schedule', {
	  templateUrl: 'components/schedule/ScheduleView.html',
	  controller: 'ScheduleController'
  });
}])

.controller('ScheduleController', ["$scope", "$http", 'Notification', function($scope, $http, Notification) {
		
	$scope.getSchedule = function(day) {
		
		var req = {
				 method: 'GET',
				 url: KLEEN_BASE_LOCATION + '/schedule/' + day
		};
		
	    return $http(req)
	    .then(function(response){

	    	$scope.schedule = response.data;
	    	return response.data;
	    });
	};

	$scope.serviceAccount = function(account, serviceType) {

		 var req = {
				 method: 'POST',
				 url: KLEEN_BASE_LOCATION + '/schedule',
				 headers: {
				   'Content-Type': "application/json"
				 },
				 data: {'account':account, 'serviceRecord': serviceType}
				}
		
		$http(req).
		  success(function(data, status, headers, config) {
			  $scope.getSchedule($scope.day);
			  Notification.success({message: "schedule updated for: " + data.account.accountNumber, positionX: 'center', positionY: 'bottom'});
		  }).
		  error(function(data, status, headers, config) {
			  Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
		  });
	};
	
	$scope.day = new Date().getDay();
	$scope.getSchedule($scope.day);
	
	$scope.$watch('day', function() { $scope.getSchedule($scope.day); }, true);
}]);

