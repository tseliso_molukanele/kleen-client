'use strict';

angular.module('myApp.DashboardModule', ['ngRoute', 'uiGmapgoogle-maps'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/dashboard', {
      templateUrl: 'components/dashboard/View.html',
	  controller: 'DashboardController'
  });
}])

.controller('DashboardController', ["$scope", 'accountService', function($scope, accountService) {

    $scope.map = {center: {latitude: -25.679253, longitude: 28.175430}, zoom: 11};

    $scope.dayData = [];

    $scope.dayData.push({title: 'Sat', icon: 'http://wwrw.co.za/resources/6.png', show: true, clientCount: 0, binCount: 0, monthly: 0});
    $scope.dayData.push({title: 'Sun', icon: 'http://wwrw.co.za/resources/7.png', show: true, clientCount: 0, binCount: 0, monthly: 0});
    $scope.dayData.push({title: 'Mon', icon: 'http://wwrw.co.za/resources/1.png', show: true, clientCount: 0, binCount: 0, monthly: 0});
    $scope.dayData.push({title: 'Tue', icon: 'http://wwrw.co.za/resources/2.png', show: true, clientCount: 0, binCount: 0, monthly: 0});
    $scope.dayData.push({title: 'Wed', icon: 'http://wwrw.co.za/resources/3.png', show: true, clientCount: 0, binCount: 0, monthly: 0});
    $scope.dayData.push({title: 'Thu', icon: 'http://wwrw.co.za/resources/4.png', show: true, clientCount: 0, binCount: 0, monthly: 0});
    $scope.dayData.push({title: 'Fri', icon: 'http://wwrw.co.za/resources/5.png', show: true, clientCount: 0, binCount: 0, monthly: 0});

    $scope.totalClients = 0;
    $scope.totalBins = 0;
    $scope.totalMonthly = 0;

    var prepareMarkers = function(accounts) {
        var counter = 0;
        for (var key in accounts) {

            var account = accounts[key];

            if (account.status == 'ACTIVE') {

                var marker = {
                    "id": ++counter,
                    "coords": {
                        "latitude": account.address.latitude,
                        "longitude": account.address.longitude
                    },
                    "icon": {"url": $scope.dayData[account.dayOfCleaning].icon},
                    title: account.displayName,
                    options: {"visible": $scope.dayData[account.dayOfCleaning].show}
                };

                $scope.$watch('dayData[' + account.dayOfCleaning + '].show', function(newVal, oldVal) {

                    marker.options.visible = newVal;

                    $scope.totalClients = 0;
                    $scope.totalBins = 0;
                    $scope.totalMonthly = 0;
                    for(var index = 0;index < 7;index++) {

                        $scope.totalClients += ($scope.dayData[index].show ? $scope.dayData[index].clientCount : 0);
                        $scope.totalBins += ($scope.dayData[index].show ? $scope.dayData[index].binCount : 0);
                        $scope.totalMonthly += ($scope.dayData[index].show ? $scope.dayData[index].monthly : 0);
                    }
                });

                $scope.dayData[account.dayOfCleaning].clientCount++;
                $scope.dayData[account.dayOfCleaning].binCount += account.plan.numberOfBins;
                $scope.dayData[account.dayOfCleaning].monthly += account.plan.monthly;

                $scope.markers.push(marker);
            }
        }
    }

    $scope.markers = [];

    var accounts = accountService.getAccounts('', 'ALL').then(function(response) {

        prepareMarkers(response);
    });

}]);
