'use strict';

angular.module('myApp.AccountformController', ['ngRoute'])

	.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
			.when('/accountform/:accountNumber', {
				templateUrl: 'components/accountform/AccountformView.html',
				controller: 'AccountformController'
			});
	}])

	.controller('AccountformController', ["$scope", "$http", '$routeParams', 'accountService', 'Notification', function ($scope, $http, $routeParams, accountService, Notification) {

		var req = {
	       method: 'GET',
	       url: KLEEN_BASE_LOCATION + '/plan'
	  };

	  $http(req)
	    .then(function(response){

	    $scope.plans = response.data;
	    });

		$scope.viewFocused = function() {

			if(!$scope.focusedProcessed) {

				datepickr('commencementdate', { dateFormat: 'Y-m-d'})
				$scope.focusedProcessed = true;

//				document.querySelector(".calendar").style.display = 'block';
			}
		}

		if ($routeParams.accountNumber.toUpperCase() !== 'NEW') {

			accountService.getAccount($routeParams.accountNumber).
				success(function (data, status, headers, config) {
					$scope.account = data;
				});
		} else {

			$scope.account = { "accountHolder": { "phoneNumbers": [], "emailAddresses": [] } };
		}

		$scope.submit = function (account) {

			var request = null;

			if (account.accountNumber) {
				request = accountService.putAccount(account);
				request.success(function (data, status, headers, config) {
					Notification.success({message: "Account updated successfully!", positionX: 'center', positionY: 'bottom'});
				})
			} else {
				request = accountService.postAccount(account);
				request.success(function (data, status, headers, config) {
					Notification.success({message: "New account number:" + data.accountNumber, positionX: 'center', positionY: 'bottom'});
					$scope.account = { "accountHolder": { "phoneNumbers": [], "emailAddresses": [] } };
				})
			}

			request
				.error(function (data, status, headers, config) {
					if (data.message) {
						Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
					} else if (data) {
						Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
					} else {
						Notification.error({message: "Error: " + status, positionX: 'center', positionY: 'bottom'});
					}
				});
		}

		$scope.activate = function (account) {

			$scope.moveStatus(account, true);
		}

		$scope.suspend = function (account) {

			$scope.moveStatus(account, false);
		}

		$scope.moveStatus = function (account, moveUp) {

			var request = accountService.putActivity(account, moveUp);
			request
				.success(function (data, status, headers, config) {
					Notification.success({message: "Account updated successfully!", positionX: 'center', positionY: 'bottom'});
					$scope.account = data;
				})
				.error(function (data, status, headers, config) {
					if (data.message) {
						Notification.error({message: data.message, positionX: 'center', positionY: 'bottom'});
					} else if (data) {
						Notification.error({message: data, positionX: 'center', positionY: 'bottom'});
					} else {
						Notification.error({message: "Error: " + status, positionX: 'center', positionY: 'bottom'});
					}
				});
		}

	}]);
